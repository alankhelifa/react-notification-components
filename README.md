# react-notification-components
This package provides components to generate and display notifications in React.

## Pre-requisites
React version >= **16.8**

## How to use
In your React project run
```bash
npm i react-notification-components
```
In your index.js import the required css
```js
import 'react-notification-components/dist/index.css';
```

Then mount the NotificationsProvider where you see fit, for example at the root of the app
```js
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'react-notification-components/dist/index.css';
import { NotificationsProvider } from 'react-notification-components';

ReactDOM.render(
  <NotificationsProvider>
    <App />
  </NotificationsProvider>,
  document.getElementById('root')
);
```

Add the Notifications component and use the NotificationHandler or the useNotifications hook to create new notifications. The Notifications are rendered through a [React portal](https://reactjs.org/docs/portals.html).
```js
import React from 'react';
import { Notifications, NotificationsHandler } from "react-notification-components";

function App() {
  return (
    <div>
      <button onClick={() => NotificationsHandler.notify('new notification')}>Notify</button>
      <Notifications />
    </div>
  );
}
```

That's it !

## Customization

You can customize the look and behavior of the notifications with props and css.

### NotificationProvider
| Props        | description     |
| -------------- | ------------- |
| defaultTimeout | Default display duration for notifications in milliseconds |
| initialNotifications | Array of notifications to be displayed on mount, each notification is shaped like <pre>{ id: string, content: ReactNode, type: ('default', 'success', 'warning', 'error'), timeout: number }</pre> |

### Notifications
| Props        | description     |
| -------------- | ------------- |
| className | A string class name |

You can customize the appearance of the notifications with simple css.

Example:
```js
...
<Notifications className="custom" />
...
```

```css
.rnc-notifications.custom {
  top: unset;
  bottom: 16px;
  font-family: sans-serif;
  font-size: 1rem;
  --default-progress: white;
}

.rnc-notifications.custom > li {
  min-height: 80px;
  background-image: linear-gradient(to right top, #8400c7, #9900c0, #aa09ba, #b917b4, #c525af, #d228a6, #dd2e9d, #e63795, #f03d86, #f64678, #fa526b, #fb5f5f);
}
```

In your custom css, you can override some variables as in the previous example

| css variables  | description     |
| -------------- | ------------- |
| --default-progress | background of the notification progress bar |
| --success-progress | background of the success notification progress bar |
| --warning-progress | background of the warning notification progress bar |
| --error-progress | background of the error notification progress bar |
| --success-bg | background of the success notification |
| --warning-bg | background of the warning notification |
| --error-bg | background of the error notification |
| --success-color | Text color of the success notification |
| --warning-color | Text color of the warning notification |
| --error-color | Text color of the error notification |






