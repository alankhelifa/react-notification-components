import { ReactNode } from 'react';

export type NOTIFICATION_TYPES = 'default' | 'success' | 'warning' | 'error';

export type NotificationProps = {
  id: string;
  timeout: number;
  type: NOTIFICATION_TYPES;
  content: ReactNode;
};

export type NotificationsProps = {
  className?: string;
};

export type NotificationContextProps = {
  children?: ReactNode;
  initialNotifications?: Array<NotificationProps>;
  defaultTimeout?: number;
};

export type NotificationContextType = {
  notifications: Array<NotificationProps>;
  notify: (content: ReactNode, type?: NOTIFICATION_TYPES, timeout?: number) => void;
  hide: (id: string) => void;
};
