import React, { createContext, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import {
  NOTIFICATION_TYPES,
  NotificationContextProps,
  NotificationContextType,
  NotificationProps,
} from './Notifications.types';
import { NotificationActionType } from './NotificationsHandler';
import NotificationDispatcher from './NotificationDispatcher';

export const NotificationsContext = createContext<NotificationContextType>({
  notifications: [],
  notify: () => {},
  hide: () => {},
});

const generateNotification = (
  content: React.ReactNode,
  type: NOTIFICATION_TYPES | undefined,
  timeout: number | undefined,
  defaultTimeout: number,
): NotificationProps => {
  const notification: Partial<NotificationProps> = { id: String(new Date().getTime()) };
  if (content) {
    notification.content = content;
  } else {
    throw new Error('Invalid value for notification content');
  }

  if (type) {
    if (['default', 'success', 'warning', 'error'].includes(type)) {
      notification.type = type;
    } else {
      throw new Error('Invalid value for alert type');
    }
  } else {
    notification.type = 'default';
  }

  if (timeout) {
    if (timeout >= 1000) {
      notification.timeout = timeout;
    } else {
      throw new Error('Invalid value for alert timeout');
    }
  } else {
    notification.timeout = defaultTimeout;
  }

  return notification as NotificationProps;
};

export const NotificationsProvider: React.FC<NotificationContextProps> = ({
  children,
  initialNotifications,
  defaultTimeout = 5000,
}) => {
  const [notifications, setNotifications] = useState(initialNotifications || []);

  const hide = useCallback(id => setNotifications(prevNotifications => prevNotifications.filter(n => n.id !== id)), [
    setNotifications,
  ]);

  const notify = useCallback(
    (content, type, timeout) => {
      setNotifications(prevNotifications =>
        prevNotifications.concat(generateNotification(content, type, timeout, defaultTimeout)),
      );
    },
    [setNotifications, defaultTimeout],
  );

  const handler = useCallback(
    ({ type, data }) => {
      if (type === NotificationActionType.NOTIFY) {
        const { content, type, timeout } = data;
        notify(content, type, timeout);
      } else if (type === NotificationActionType.HIDE) {
        const { id } = data;
        hide(id);
      }
    },
    [hide, notify],
  );

  useEffect(() => {
    NotificationDispatcher.register(handler);
  }, [handler]);

  const store = useMemo(
    () => ({
      notifications,
      hide,
      notify,
    }),
    [notifications, hide, notify],
  );

  return <NotificationsContext.Provider value={store}>{children}</NotificationsContext.Provider>;
};

export const useNotifications = (): NotificationContextType => useContext(NotificationsContext);
