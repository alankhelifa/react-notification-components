class NotificationDispatcher {
  _callback: any;

  constructor() {
    this._callback = null;
  }

  register(callback: any) {
    this._callback = callback;
  }

  dispatch(payload: any) {
    if (this._callback) {
      this._callback(payload);
    }
  }
}

export default new NotificationDispatcher();
