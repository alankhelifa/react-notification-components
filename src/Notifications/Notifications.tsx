import React from 'react';
import { AnimatePresence } from 'framer-motion';
import { Notification } from './Notification';
import { useNotifications } from 'Notifications/NotificationContext';
import { NotificationsProps } from 'Notifications/Notifications.types';
import './Notification.scss';
import { withPortal } from 'Portal/withPortal';

const NotificationsBase = ({ className }: NotificationsProps) => {
  const { notifications } = useNotifications();

  return (
    <ul className={`rnc-notifications ${className ? className : ''}`}>
      <AnimatePresence>
        {notifications.map(notification => (
          <Notification key={notification.id} {...notification} />
        ))}
      </AnimatePresence>
    </ul>
  );
};

//@ts-ignore
export const Notifications: (props: NotificationsProps) => React.ReactElement = withPortal('root')(NotificationsBase);
