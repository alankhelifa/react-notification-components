import React, { ReactElement, useCallback, useEffect, useState } from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import { useNotifications } from 'Notifications/NotificationContext';
import { NotificationProps } from 'Notifications/Notifications.types';
import ClearIcon from 'assets/clear.svg';
import ErrorIcon from 'assets/error.svg';
import SuccessIcon from 'assets/success.svg';
import WarningIcon from 'assets/warning.svg';
import './Notification.scss';

const variants = {
  init: {
    opacity: 0,
    x: 100,
  },
  enter: {
    opacity: 1,
    x: 0,
    transition: {
      x: { type: 'spring', stiffness: 400, damping: 30 },
      opacity: { duration: 0.3 },
    },
  },
  exit: {
    opacity: 0,
    scale: 0.6,
    transition: {
      duration: 0.3,
    },
  },
};

const progressVariants = {
  init: {
    width: '100%',
    opacity: 1,
  },
  animate: (timeout: number) => ({
    width: 0,
    transition: {
      ease: 'linear',
      duration: timeout / 1000,
    },
  }),
  exit: {
    opacity: 0,
    transition: {
      duration: 0.3,
    },
  },
};

const ICONS = {
  success: SuccessIcon,
  warning: WarningIcon,
  error: ErrorIcon,
};

export const Notification = ({ id, content, type, timeout }: NotificationProps): ReactElement => {
  const { hide } = useNotifications();
  const [paused, setPaused] = useState(false);

  useEffect(() => {
    let timer: NodeJS.Timeout;

    if (!paused && type !== 'error') {
      timer = setTimeout(() => {
        hide(id);
      }, timeout);
    }

    return () => timer && clearTimeout(timer);
  }, [id, hide, paused, timeout, type]);

  const pause = useCallback(() => {
    if (type !== 'error') {
      setPaused(true);
    }
  }, [type]);

  const resume = useCallback(() => {
    if (type !== 'error') {
      setPaused(false);
    }
  }, [type]);

  const remove = useCallback(() => {
    hide(id);
  }, [id, hide]);

  // @ts-ignore
  const Icon = ICONS[type];
  return (
    <motion.li
      key={id}
      className={`rnc-notification ${type}`}
      onMouseEnter={pause}
      onMouseLeave={resume}
      initial="init"
      animate="enter"
      exit="exit"
      layout="position"
      variants={variants}
    >
      {type !== 'default' && (
        <span className="icon">
          <Icon height={28} width={28} />
        </span>
      )}
      <span>{content}</span>
      <ClearIcon onClick={remove} height={20} width={20} className={`remove${type === 'error' ? ' show' : ''}`} />
      {type !== 'error' && (
        <AnimatePresence>
          {!paused && (
            <motion.div
              className="progress"
              animate="animate"
              initial="init"
              exit="exit"
              variants={progressVariants}
              custom={timeout}
            />
          )}
        </AnimatePresence>
      )}
    </motion.li>
  );
};
