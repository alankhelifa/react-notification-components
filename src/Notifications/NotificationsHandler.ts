import { ReactNode } from 'react';
import Dispatcher from './NotificationDispatcher';
import { NOTIFICATION_TYPES } from './Notifications.types';

export enum NotificationActionType {
  NOTIFY = 'notify',
  HIDE = 'hide',
}

class Handler {
  notify(content: ReactNode, type?: NOTIFICATION_TYPES, timeout?: number) {
    Dispatcher.dispatch({
      type: NotificationActionType.NOTIFY,
      data: { content, type, timeout },
    });
  }

  hide(id: string) {
    Dispatcher.dispatch({
      type: NotificationActionType.HIDE,
      data: { id },
    });
  }
}

export const NotificationsHandler = new Handler();
