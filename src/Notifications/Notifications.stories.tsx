import React, { useCallback } from 'react';
import { NotificationsProvider } from './NotificationContext';
import { Notifications } from 'Notifications/Notifications';
import { NotificationsHandler } from './NotificationsHandler';

export default {
  title: 'Notifications',
};

const NotificationsExample = (): React.ReactElement => {
  const createDefaultNotification = useCallback(() => {
    NotificationsHandler.notify('New notification');
  }, []);
  const createSuccessNotification = useCallback(() => {
    NotificationsHandler.notify('New notification', 'success');
  }, []);
  const createWarningNotification = useCallback(() => {
    NotificationsHandler.notify('New notification', 'warning');
  }, []);
  const createErrorNotification = useCallback(() => {
    NotificationsHandler.notify('New notification', 'error');
  }, []);

  return (
    <div>
      <button onClick={createDefaultNotification}>notify</button>
      <button onClick={createSuccessNotification}>success</button>
      <button onClick={createWarningNotification}>warning</button>
      <button onClick={createErrorNotification}>error</button>
      <Notifications />
    </div>
  );
};

export const Example = (): React.ReactElement => {
  return (
    <NotificationsProvider defaultTimeout={10000}>
      <NotificationsExample />
    </NotificationsProvider>
  );
};

const Customization = (): React.ReactElement => {
  const createDefaultNotification = useCallback(() => {
    NotificationsHandler.notify('New notification');
  }, []);

  return (
    <div style={{ height: 3000 }}>
      <div>
        <button onClick={createDefaultNotification}>notify</button>
      </div>
      <div>
        <p>Notifications will follow the scroll</p>
      </div>

      <Notifications className="custom" />
    </div>
  );
};

export const CustomizationExample = (): React.ReactElement => {
  return (
    <NotificationsProvider defaultTimeout={10000}>
      <Customization />
    </NotificationsProvider>
  );
};
