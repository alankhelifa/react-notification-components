import React from 'react';
import ReactDOM from 'react-dom';

export const withPortal = (targetId: string) => (Component: React.ReactNode) => {
  return class Portal extends React.Component {
    target: HTMLElement | null;
    readonly wrapper: HTMLDivElement;

    constructor(props: Readonly<never>) {
      super(props);

      const target = document.getElementById(targetId);

      if (target === null) {
        throw new Error(`No element with id ${targetId} found`);
      }

      this.target = target;
      this.wrapper = document.createElement('div');
      target.appendChild(this.wrapper);
    }

    componentWillUnmount() {
      this.target!.removeChild(this.wrapper);
    }

    render() {
      // @ts-ignore
      return ReactDOM.createPortal(<Component {...this.props} />, this.wrapper);
    }
  };
};
