const path = require('path');

module.exports = {
  stories: ['../src/**/*.stories.tsx'],
  addons: [],
  webpackFinal: async (config) => {
    let rule = config.module.rules.find(
      r =>
        r.test &&
        r.test.toString().includes("svg") &&
        r.loader &&
        r.loader.includes("file-loader")
    )
    rule.test = /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|cur|ani)(\?.*)?$/

    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"],
      include: path.resolve(__dirname, '../src'),
    });
    config.resolve.modules = [
      ...(config.resolve.modules || []),
      path.resolve('./src'),
    ];
    config.module.rules.push({
      test: /\.scss$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
      include: path.resolve(__dirname, '../src'),
    });
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      loader: require.resolve('babel-loader'),
      options: {
        presets: [['react-app', { flow: false, typescript: true }]],
      },
    });
    config.resolve.extensions.push('.ts', '.tsx');

    return config;
  },
};
